var email, sign_up, item;

function init() {
    email = document.getElementById("email");
    sign_up = document.getElementById("signup");
    var forms = document.getElementsByClassName("form-group");
    console.log(sign_up);

}

function submit() {
    for (item in document.forms.regform.getElementsByClassName("form-control")) {
        if (item.value == null) {
            alert("Value missing!" + item.name);
            console.log("Error, " + item.name);
            return false;
        }
    }
    if (!email.value.match("/.+@.+\.edu$/")) {
        alert("Not an .edu email address!");
        return false;
    }
    else if (false) {

    }

    return false;
}

function check() {
    if (!document.getElementById("signup").checked) {
        document.getElementById("submitButton").disabled = true;
        console.log("Disabled button");
    }
    else {
        document.getElementById("submitButton").disabled = false;
        console.log("Enabled button");
    }
}
